import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  items: any;

  constructor(private newsService: NewsService, private router: Router, private menu: MenuController) {}

  ngOnInit() {
    this.newsService.getData('top-headlines?country=us&category=general')
    .subscribe(data =>{
      this.items = data;
    });
  }

  openNews(article) {
    this.newsService.currentNews = article;
    this.router.navigate(['/details']);
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
}
